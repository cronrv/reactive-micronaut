package com.concurrency.service.infrastructure.rest;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@MicronautTest
class GatewayTokenSecureTest {

    @Inject
    private GatewayTokenRestClient gatewayTokenSecure;

    @Test
    void getGatewayToken() {
        assertDoesNotThrow(() -> gatewayTokenSecure.getToken());
    }
}