package com.concurrency.service.repository;

import com.concurrency.service.infrastructure.rest.NumberRestClient;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

@AllArgsConstructor
public class ReactivePrimeNumberRepository implements NumberRepository<Flux<String>> {

    private final NumberRestClient<Mono<String>> numberRestClient;

    public Flux<String> getNumbers(int range) {
        return Flux.fromIterable(IntStream.range(0, range)
                .mapToObj(n -> numberRestClient.getPrimeFromOutside())
                        .collect(Collectors.toList()))
                .map(Mono::block);
    }
}
