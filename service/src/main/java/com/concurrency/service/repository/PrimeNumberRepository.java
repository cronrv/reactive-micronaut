package com.concurrency.service.repository;

import com.concurrency.service.infrastructure.rest.NumberRestClient;
import jakarta.inject.Inject;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@AllArgsConstructor(onConstructor_= {@Inject})
public class PrimeNumberRepository implements NumberRepository<List<String>> {

    private final NumberRestClient<String> numberRestClient;

    public List<String> getNumbers(int range) {
        return IntStream.range(0, range)
                .parallel()
                .mapToObj(list -> numberRestClient.getPrimeFromOutside())
                .collect(Collectors.toList());
    }
}
