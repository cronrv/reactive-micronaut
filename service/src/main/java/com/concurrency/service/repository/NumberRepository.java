package com.concurrency.service.repository;

public interface NumberRepository<T> {
    T getNumbers(int range);
}
