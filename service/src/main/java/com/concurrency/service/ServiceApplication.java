package com.concurrency.service;

import io.micronaut.runtime.Micronaut;

public class ServiceApplication {

	public static void main(String[] args) {
		Micronaut.run(ServiceApplication.class, args);
	}

}
