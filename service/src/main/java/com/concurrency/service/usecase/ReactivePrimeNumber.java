package com.concurrency.service.usecase;

import com.concurrency.service.repository.NumberRepository;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class ReactivePrimeNumber implements NumberUsecase<Flux<String>>{

    private final NumberRepository<Flux<String>> numberRepository;

    @Override
    public Flux<String> getNumbers(int range) {
        return numberRepository.getNumbers(range);
    }
}
