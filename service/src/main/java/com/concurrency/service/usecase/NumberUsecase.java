package com.concurrency.service.usecase;

public interface NumberUsecase<T> {
    T getNumbers(int range);
}
