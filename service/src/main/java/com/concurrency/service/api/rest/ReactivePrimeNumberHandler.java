package com.concurrency.service.api.rest;

import com.concurrency.service.usecase.NumberUsecase;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.inject.Inject;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;

@Controller("/api/v2")
@AllArgsConstructor
public class ReactivePrimeNumberHandler implements NumberRestEntryPoint<Flux<String>, String> {

    @Inject
    private final NumberUsecase<Flux<String>> primeNumber;

    @ExecuteOn(TaskExecutors.IO)
    @Override
    @Get("/prime/numbers")
    public Flux<String> getNumbers(@QueryValue String range) {
        return primeNumber.getNumbers(Integer.parseInt(range));
    }
}
