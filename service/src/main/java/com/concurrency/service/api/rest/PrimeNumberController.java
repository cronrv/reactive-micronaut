package com.concurrency.service.api.rest;

import com.concurrency.service.usecase.NumberUsecase;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.inject.Inject;
import lombok.AllArgsConstructor;

import java.util.List;

@Controller("/api/v1")
@AllArgsConstructor
public class PrimeNumberController implements NumberRestEntryPoint<List<String>, String> {

    @Inject
    private final NumberUsecase<List<String>> primeNumber;

    @ExecuteOn(TaskExecutors.IO)
    @Override
    @Get("/prime/numbers")
    public List<String> getNumbers(@QueryValue String range) {
        return primeNumber.getNumbers(Integer.parseInt(range));
    }
}
