package com.concurrency.service.infrastructure.rest;

import com.concurrency.service.entity.GatewayToken;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class ReactivePrimeNumberRestClient implements NumberRestClient<Mono<Object>> {

    private final SecurityTokenRestClient<Mono<GatewayToken>> gatewayTokenSecure;

    private final String clientIdHeader;
    private final String clientId;
    private final String gatewayHost;
    private final String primeMethod;
    private final String tokenParam;

    private final HttpClient httpClient;

    public Mono<Object> getPrimeFromOutside() {
        Mono<GatewayToken> token;
        if(!gatewayTokenSecure.validateToken()) {
            token = gatewayTokenSecure.getToken();
        } else {
            token= gatewayTokenSecure.getCachedToken();
        }

        return token.map(tokenResponse ->
                Mono.from(httpClient.retrieve(HttpRequest.GET(getUri(tokenResponse)).header(clientIdHeader, clientId),
                    Argument.of(String.class))).block());
    }

    private String getUri(GatewayToken token) {
        return new StringBuffer(gatewayHost)
                .append(primeMethod)
                .append(tokenParam)
                .append(token.getToken())
                .toString();
    }
}
