package com.concurrency.service.infrastructure.rest;

public interface NumberRestClient<T> {
    T getPrimeFromOutside();
}
