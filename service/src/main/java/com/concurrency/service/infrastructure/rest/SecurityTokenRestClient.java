package com.concurrency.service.infrastructure.rest;

public interface SecurityTokenRestClient<T> {
    T getCachedToken();
    boolean validateToken(T token);
    boolean validateToken();
    T getToken();
    default void setToken(){}
}
