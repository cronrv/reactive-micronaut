package com.concurrency.service.infrastructure.rest;

import com.concurrency.service.api.exceptions.ServiceException;
import com.concurrency.service.entity.GatewayToken;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class PrimeNumberRestClient implements NumberRestClient<String>{

    private final SecurityTokenRestClient<GatewayToken> gatewayTokenSecure;

    private final String clientIdHeader;
    private final String clientId;
    private final String gatewayHost;
    private final String primeMethod;
    private final String tokenParam;

    private final HttpClient httpClient;

    public String getPrimeFromOutside() {
        GatewayToken token = gatewayTokenSecure.getCachedToken();

        HttpRequest<?> request = HttpRequest.GET(getUri(token))
                .header(clientIdHeader, clientId);

        String primeNumberResponse = Mono.from(
                httpClient.retrieve(request, Argument.of(String.class))).block();

        if(primeNumberResponse == null){
            throw new ServiceException("Bad gateway response received");
        }

        return primeNumberResponse;
    }

    private String getUri(GatewayToken token) {
        return new StringBuffer(gatewayHost)
                .append(primeMethod)
                .append(tokenParam)
                .append(token.getToken())
                .toString();
    }
}
