package com.concurrency.service.infrastructure.rest;

import com.concurrency.service.api.exceptions.ServiceException;
import com.concurrency.service.entity.GatewayToken;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import reactor.core.publisher.Mono;

import java.time.Instant;

@RequiredArgsConstructor
@Slf4j
public class GatewayTokenRestClient implements SecurityTokenRestClient<GatewayToken> {

    private final GatewayToken gatewayToken = new GatewayToken();

    @NonNull
    private final String clientIdHeader;
    @NonNull
    private final String clientId;
    @NonNull
    private final String gatewayHost;
    @NonNull
    private final String tokenMethod;
    @NonNull
    private HttpClient httpClient;

    public synchronized GatewayToken getCachedToken() {
        if(gatewayToken.getToken() == null || !validateToken(gatewayToken)){
            getToken();
        }
        return new ModelMapper().map(gatewayToken, GatewayToken.class);
    }

    public synchronized boolean validateToken(GatewayToken gatewayToken) {
        return Instant.now()
                .isBefore(gatewayToken.getIssuedTime().plusMillis(gatewayToken.getExpirationTime().toEpochMilli()) );
    }

    @Override
    public boolean validateToken() {
        return validateToken(this.gatewayToken);
    }

    public GatewayToken getToken() {
        log.info("Requesting a new token, Thread: " + Thread.currentThread().getId());

        HttpRequest<?> request = HttpRequest.GET(tokenMethod)
                .header(clientIdHeader, clientId);

        return Mono.from(
                        httpClient.retrieve(request, Argument.of(GatewayToken.class)))
                .doOnSuccess(token -> {
                    if(token == null){
                        throw new ServiceException("Bad gateway response received");
                    }
                    new ModelMapper().map(token, gatewayToken);
                }).block();
    }

}
