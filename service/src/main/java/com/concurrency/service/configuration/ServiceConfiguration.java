package com.concurrency.service.configuration;

import com.concurrency.service.entity.GatewayToken;
import com.concurrency.service.infrastructure.rest.GatewayTokenRestClient;
import com.concurrency.service.infrastructure.rest.NumberRestClient;
import com.concurrency.service.infrastructure.rest.PrimeNumberRestClient;
import com.concurrency.service.infrastructure.rest.SecurityTokenRestClient;
import com.concurrency.service.repository.NumberRepository;
import com.concurrency.service.repository.PrimeNumberRepository;
import com.concurrency.service.usecase.NumberUsecase;
import com.concurrency.service.usecase.PrimeNumber;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import java.util.List;

@Factory
public class ServiceConfiguration {

    @Value("${gateway.rest-client.client-id.header}")
    private String clientIdHeader;
    @Value("${gateway.rest-client.client-id.value}")
    private String clientId;
    @Value("${gateway.rest-client.host}")
    private String gatewayHost;
    @Value("${gateway.rest-client.token-method}")
    private String tokenMethod;
    @Value("${gateway.prime-number-method}")
    private String primeMethod;
    @Value("${gateway.prime-number-token-param}")
    private String tokenParam;

    @Inject
    @Client("http://localhost:8081")
    private HttpClient httpClient;

    @Singleton
    public NumberUsecase<List<String>> getPrimeNumber(
            @InjectorsQualifiers.Repository(value = "primeNumber")
                    NumberRepository<List<String>> numberRepository) {
        return new PrimeNumber(numberRepository);
    }

    @Singleton
    @InjectorsQualifiers.Repository(value = "primeNumber")
    public NumberRepository<List<String>> getPrimeNumberRepository(
            @InjectorsQualifiers.RestClient(value = "primeNumber")
                    NumberRestClient<String> numberRestClient) {
        return new PrimeNumberRepository(numberRestClient);
    }

    @Singleton
    @InjectorsQualifiers.RestClient(value = "primeNumber")
    public NumberRestClient<String> getPrimeNumberRepository(
            SecurityTokenRestClient<GatewayToken> securityTokenRestClient) {
        return new PrimeNumberRestClient(securityTokenRestClient,
                clientIdHeader, clientId, gatewayHost, primeMethod, tokenParam, httpClient);
    }

    @Singleton
    public SecurityTokenRestClient<GatewayToken> getGatewayTokenRestClient() {
        return new GatewayTokenRestClient(clientIdHeader, clientId, gatewayHost, tokenMethod, httpClient);
    }
}
