package com.concurrency.service.configuration;

import com.concurrency.service.entity.GatewayToken;
import com.concurrency.service.infrastructure.rest.NumberRestClient;
import com.concurrency.service.infrastructure.rest.ReactiveGatewayTokenRestClient;
import com.concurrency.service.infrastructure.rest.ReactivePrimeNumberRestClient;
import com.concurrency.service.infrastructure.rest.SecurityTokenRestClient;
import com.concurrency.service.repository.NumberRepository;
import com.concurrency.service.repository.ReactivePrimeNumberRepository;
import com.concurrency.service.usecase.NumberUsecase;
import com.concurrency.service.usecase.ReactivePrimeNumber;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Factory
public class ReactiveServiceConfiguration {

    @Value("${gateway.rest-client.client-id.header}")
    private String clientIdHeader;
    @Value("${gateway.rest-client.client-id.value}")
    private String clientId;
    @Value("${gateway.rest-client.host}")
    private String gatewayHost;
    @Value("${gateway.rest-client.token-method}")
    private String tokenMethod;
    @Value("${gateway.prime-number-method}")
    private String primeMethod;
    @Value("${gateway.prime-number-token-param}")
    private String tokenParam;

    @Inject
    @Client("http://localhost:8081")
    private HttpClient httpClient;

    @Singleton
    @InjectorsQualifiers.RestClient(value = "reactivePrimeNumber")
    public NumberUsecase<Flux<String>> getPrimeNumber(
            @InjectorsQualifiers.RestClient("reactivePrimeNumberRepository")
                    NumberRepository<Flux<String>> numberRepository) {
        return new ReactivePrimeNumber(numberRepository);
    }

    @Singleton
    @InjectorsQualifiers.RestClient(value = "reactivePrimeNumberRepository")
    public NumberRepository<Flux<String>> getPrimeNumberRepository(
            @InjectorsQualifiers.RestClient(value = "reactivePrimeNumberRestClient")
                    NumberRestClient<Mono<String>> numberRestClient) {
        return new ReactivePrimeNumberRepository(numberRestClient);
    }

    @Singleton
    @InjectorsQualifiers.RestClient(value = "reactivePrimeNumberRestClient")
    public NumberRestClient<Mono<Object>> getReactiveRestClient(
            @InjectorsQualifiers.RestClient(value = "reactiveGatewayTokenRestClient")
                    SecurityTokenRestClient<Mono<GatewayToken>> securityTokenRestClient) {
        return new ReactivePrimeNumberRestClient(securityTokenRestClient,
                clientIdHeader, clientId, gatewayHost, primeMethod, tokenParam, httpClient);
    }

    @Singleton
    @InjectorsQualifiers.RestClient(value = "reactiveGatewayTokenRestClient")
    public SecurityTokenRestClient<Mono<GatewayToken>> getGatewayTokenRestClient() {
        return new ReactiveGatewayTokenRestClient(clientIdHeader, clientId, gatewayHost, tokenMethod, httpClient);
    }
}
