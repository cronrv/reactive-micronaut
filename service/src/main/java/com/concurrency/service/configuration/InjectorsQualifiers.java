package com.concurrency.service.configuration;

import jakarta.inject.Qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface InjectorsQualifiers {

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @interface Repository{
        String value();
    }

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @interface RestClient{
        String value();
    }
}
