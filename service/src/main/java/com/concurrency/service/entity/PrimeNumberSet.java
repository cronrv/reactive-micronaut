package com.concurrency.service.entity;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class PrimeNumberSet {
    private List<String> primeNumbers;
    private int order;
    private int listNumber;

    @Override
    public String toString() {
        String primeNumbersField = "primeNumber=";
        return "PrimeNumberSet{" +
                "order=" + order + ", \n" +
                "listNumber=" + listNumber + ", \n" +
                primeNumbers.stream()
                        .map(primeNumber -> primeNumbersField + primeNumber + "\n")
                        .collect(Collectors.joining()) +
                '}';
    }
}
