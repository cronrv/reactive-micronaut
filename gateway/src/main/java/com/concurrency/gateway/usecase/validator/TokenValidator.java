package com.concurrency.gateway.usecase.validator;

import com.concurrency.gateway.usecase.ServiceChain;
import com.concurrency.gateway.util.ValidatorType;
import jakarta.inject.Inject;
import lombok.AllArgsConstructor;

@AllArgsConstructor(onConstructor_= {@Inject})
public class TokenValidator implements ServiceChain<Boolean, String, ValidatorType> {

    private final ServiceChain<Boolean, String, ValidatorType> defaultValidator;

    @Override
    public Boolean processData(String requestData) {
        return true;
    }

    @Override
    public Boolean checkChainElement(String requestData, ValidatorType type) {
        return type.equals(ValidatorType.TOKEN)
                ? processData(requestData)
                : defaultValidator.checkChainElement(requestData, type);
    }
}
