package com.concurrency.gateway.usecase.number;

import com.concurrency.gateway.api.exceptions.ServiceException;
import com.concurrency.gateway.entity.Credentials;
import com.concurrency.gateway.usecase.Service;
import com.concurrency.gateway.usecase.ServiceChain;
import com.concurrency.gateway.util.ValidatorType;
import jakarta.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

@AllArgsConstructor(onConstructor_= {@Inject})
@Slf4j
public class PrimeNumberService implements Service<String, Credentials> {

    private final ServiceChain<Boolean, String, ValidatorType> validatorHook;

    private final int limit;
    private final int waitInMs;

    public String processData(Credentials credentials) {

        validateCredentials(credentials);

        Boolean[] isPrime = new Boolean[limit];
        Arrays.fill(isPrime, true);

        IntStream.range(2, (int)Math.sqrt(limit))
                .forEach(n -> testIntegers(isPrime, n));

        try {
            Thread.sleep(waitInMs);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }

        return IntStream.range(2, limit)
                .map(n -> Boolean.TRUE.equals(isPrime[n]) ? n : -1)
                .filter(n -> n != -1)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll).toString();
    }

    private void validateCredentials(Credentials credentials) {
        if (Boolean.FALSE.equals(validatorHook.checkChainElement(credentials.getClientId(), ValidatorType.CLIENT_ID))){
            throw new ServiceException("Bad client id");
        }
        if(Boolean.FALSE.equals(validatorHook.checkChainElement(credentials.getToken(), ValidatorType.TOKEN))){
            throw new ServiceException("Bad token");
        }
    }

    private void testIntegers(Boolean[] isPrime, int i) {
        for(int n = i + i; n < isPrime.length; n = n + i) {
            isPrime[n] = false;
        }
    }
}
