package com.concurrency.gateway.usecase;

public interface ServiceChain<T, U, V> extends Service<T, U> {
    T checkChainElement(U requestData, V type);
}
