package com.concurrency.gateway.usecase.validator;

import com.concurrency.gateway.usecase.ServiceChain;
import com.concurrency.gateway.util.ValidatorType;

public class DefaultValidator implements ServiceChain<Boolean, String, ValidatorType> {

    @Override
    public Boolean processData(String requestData) {
        return false;
    }

    @Override
    public Boolean checkChainElement(String requestData, ValidatorType type) {
        return processData(requestData);
    }
}
