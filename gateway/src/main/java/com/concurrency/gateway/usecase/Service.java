package com.concurrency.gateway.usecase;

public interface Service<T, U> {
    T processData(U requestData);
}
