package com.concurrency.gateway.entity;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class PrimeNumberSet {
    private List<String> primeNumbers;
    private int order;

    @Override
    public String toString() {
        String primeNumbersField = "primeNumber=";
        return "PrimeNumberSet{" +
                primeNumbers.stream()
                        .map(primeNumber -> primeNumbersField + primeNumber + "\n")
                        .collect(Collectors.joining()) +
                ", order=" + order +
                '}';
    }
}
