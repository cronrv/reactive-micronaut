package com.concurrency.gateway.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Credentials {
    private String clientId;
    private String token;
}
