package com.concurrency.gateway.api.rest;

import com.concurrency.gateway.entity.Credentials;
import com.concurrency.gateway.entity.GatewayToken;
import com.concurrency.gateway.usecase.Service;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Controller("/v1/gateway")
@AllArgsConstructor
@Slf4j
public class GatewayController {

    @Inject
    private final Service<GatewayToken, String> gatewayService;
    @Inject
    private final Service<String, Credentials> primeNumberService;

    @ExecuteOn(TaskExecutors.IO)
    @Get(value = "/token")
    public HttpResponse<GatewayToken> getToken(
            @Header(value = "client_id") String clientId) {
        return HttpResponse.ok(gatewayService.processData(clientId));
    }

    @ExecuteOn(TaskExecutors.IO)
    @Get("/primeNumbers")
    public HttpResponse<String> getPrimeNumbers(
            @QueryValue String token,
            @Header(value = "client_id") String clientId) {
        return HttpResponse.ok(
                primeNumberService.processData(
                        Credentials.builder()
                                .token(token)
                                .clientId(clientId)
                                .build()));
    }
}
