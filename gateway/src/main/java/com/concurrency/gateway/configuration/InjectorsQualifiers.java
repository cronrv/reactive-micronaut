package com.concurrency.gateway.configuration;

import jakarta.inject.Qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface InjectorsQualifiers {

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @interface Service{
        String value();
    }

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @interface Chain{
        String value();
    }
}
