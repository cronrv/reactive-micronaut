package com.concurrency.gateway.configuration;

import com.concurrency.gateway.entity.Credentials;
import com.concurrency.gateway.entity.GatewayToken;
import com.concurrency.gateway.usecase.Service;
import com.concurrency.gateway.usecase.ServiceChain;
import com.concurrency.gateway.usecase.gateway.GatewayService;
import com.concurrency.gateway.usecase.number.PrimeNumberService;
import com.concurrency.gateway.usecase.validator.ClientIdValidator;
import com.concurrency.gateway.usecase.validator.DefaultValidator;
import com.concurrency.gateway.usecase.validator.TokenValidator;
import com.concurrency.gateway.usecase.validator.ValidatorHook;
import com.concurrency.gateway.util.ValidatorType;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Value;
import jakarta.inject.Singleton;

@Factory
public class GatewayConfiguration {

    @Value("${gateway.rest-client.client-id.value}")
    private String clientId;
    @Value("${gateway.rest-client.delay}")
    private int waitInMS;
    @Value("${gateway.rest-client.expiration-hours}")
    private int amountToAdd;
    @Value("${gateway.rest-client.prime-number-list-limit}")
    private int limit;

    @Singleton
    @InjectorsQualifiers.Service(value = "gateway")
    public Service<GatewayToken, String> getGatewayService(
            @InjectorsQualifiers.Chain(value = "validatorHook")
                    ServiceChain<Boolean, String, ValidatorType> validatorHook){
        return new GatewayService(validatorHook, waitInMS, amountToAdd);
    }

    @Singleton
    @InjectorsQualifiers.Service(value = "primeNumber")
    public Service<String, Credentials> getPrimeNumberService(
            @InjectorsQualifiers.Chain(value = "validatorHook")
                    ServiceChain<Boolean, String, ValidatorType> validatorHook){
        return new PrimeNumberService(validatorHook, limit, waitInMS);
    }

    @Singleton
    @InjectorsQualifiers.Chain(value = "validatorHook")
    public ServiceChain<Boolean, String, ValidatorType> getServiceChainValidatorHook(
            @InjectorsQualifiers.Chain(value = "clientIdValidator")
                    ServiceChain<Boolean, String, ValidatorType> clientIdValidator){
        return new ValidatorHook(clientIdValidator);
    }

    @Singleton
    @InjectorsQualifiers.Chain(value = "clientIdValidator")
    public ServiceChain<Boolean, String, ValidatorType> getClientIdValidator(
            @InjectorsQualifiers.Chain(value = "tokenValidator")
                    ServiceChain<Boolean, String, ValidatorType> tokenValidator){
        return new ClientIdValidator(tokenValidator, clientId);
    }

    @Singleton
    @InjectorsQualifiers.Chain(value = "tokenValidator")
    public ServiceChain<Boolean, String, ValidatorType> getTokenValidator(
            @InjectorsQualifiers.Chain(value = "defaultValidator")
                    ServiceChain<Boolean, String, ValidatorType> defaultValidator){
        return new TokenValidator(defaultValidator);
    }

    @Singleton
    @InjectorsQualifiers.Chain(value = "defaultValidator")
    public ServiceChain<Boolean, String, ValidatorType> getDefaultValidator(){
        return new DefaultValidator();
    }
}
